#include <stdio.h>
#include "mystrlen.h"

size_t mystrlen(const char* str){
	const char *str_end = str;
	while (*str_end)str_end++;
	return (str_end - str);
}


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "sort.h"

int compare(const void * left, const void * right)
{
	return ( *(int*)left - *(int*)right );
}
 

int main(void)
{	
	//Контрольный пример:
	int len = 20;
	int * array = malloc(sizeof(int) * len);
	int * array_cpy = malloc(sizeof(int) * len);
	for (int step = 1; step < 16; step++)
	{
		printf ("Step #%d: ", step);
		init_array (array, len);
		memcpy(array_cpy, array, sizeof(int) * len);
		mysort (array, len);
		qsort(array_cpy, len, sizeof(int), compare);
		printf(check_order(array, array_cpy, len) ? "Passed\n" : "FAILED\n");
	}
	free (array);
	free (array_cpy);
	return 0;
}

#include "strtrns.h"
#include <stdlib.h>
#include <assert.h>

char* strtrns(const char* string, const char* old, const char* new, char* result)
{
	assert(result);
	for(int j = 0; j < mystrlen(string); j++){
		result[j] =	string[j];
		for(int i = 0; i < mystrlen(old); i++)
		{
			if(string[j] == old[i]){
				result[j] = new[i];
			}
		}
	}
	return result;
}
